/*
    Copyright (C) 2023 Victor Pacheco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    George Porter Discord Bot
    @author vmpacheco1
    @version 0.2.1
*/

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;


public class Main {

    public static void main(String[] args) {
        String token = ""; // PUT YOUR BOT TOKEN HERE
        DiscordApi api = new DiscordApiBuilder().setToken(token).login().join();
        api.updateActivity(""); // PUT A STATUS HERE
        System.out.println("Bot invite link: " + api.createBotInvite());

        HashMap<String, Byte> votedGames = new HashMap<>();
        HashMap<String, String[]> userRedoID = new HashMap<>();
        AtomicBoolean topFourMode = new AtomicBoolean(false);

        // add a listener which answers with Pong!
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().equalsIgnoreCase(".ping")) {
                event.getChannel().deleteMessages(event.getMessage());
                event.getChannel().sendMessage("Pong!");
            }
        });

        // add a listener that starts the game picking process
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().equalsIgnoreCase(".gaming")) {
                votedGames.clear();
                userRedoID.clear();
                topFourMode.set(false);

                event.getChannel().sendMessage("Please list all games you want to play using the .add command (minimum 4 unique games)");
            }
        });

        // add a listener that adds games to the GameCount depending on the status of topFourMode
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().length() > 4 && event.getMessageContent().substring(0, 4).contains(".add")) {
                String[] addGames = event.getMessageContent().substring(5).split(" ");
                // first sequence - users add a list of games they would be willing to play
                if (!topFourMode.get()) {
                    if (addGames.length < 4) {
                        event.getChannel().deleteMessages(event.getMessage());
                        event.getChannel().sendMessage("Please list at least 4 unique games.");
                    } else if (addGames.length >= 4) {
                        addGames(votedGames, addGames);
                        userRedoID.put(event.getMessageAuthor().toString(), addGames);
                        event.getChannel().sendMessage("Games (" + event.getMessageContent().substring(5) + ") added to the list.\n" + gamesToString(votedGames));
                    }
                }

                // second sequence - users rank games from most willing to least willing
                else if (topFourMode.get()) {
                    if (addGames.length != 4) {
                        event.getChannel().deleteMessages(event.getMessage());
                        event.getChannel().sendMessage("Please rank the current games only.");
                    } else if (addGames.length == 4) {
                        topGames(votedGames, addGames);
                        userRedoID.put(event.getMessageAuthor().toString(), addGames);
                        event.getChannel().deleteMessages(event.getMessage());
                        event.getChannel().sendMessage("Games ranked.");
                    }
                }
            }
        });

        // add a listener that adds games to the GameCount depending on the status of topFourMode
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().length() > 5 && event.getMessageContent().substring(0, 5).contains(".redo")) {
                String[] addGames = event.getMessageContent().substring(6).split(" ");
                // first sequence - users add a list of games they would be willing to play
                if (!topFourMode.get()) {
                    if (addGames.length < 4) {
                        event.getChannel().deleteMessages(event.getMessage());
                        event.getChannel().sendMessage("Please list at least 4 unique games to redo.");
                    } else if (addGames.length >= 4) {
                        undoPrevVote(votedGames, userRedoID.get(event.getMessageAuthor().toString()));
                        addGames(votedGames, addGames);
                        userRedoID.put(event.getMessageAuthor().toString(), addGames);
                        event.getChannel().sendMessage("List adjusted. Games (" + event.getMessageContent().substring(6) + ") added to the list.\n" + gamesToString(votedGames));
                    }
                }

                else if (topFourMode.get()) {
                    event.getChannel().deleteMessages(event.getMessage());
                    event.getChannel().sendMessage("You cannot use .redo during ranking.");
                }
            }
        });

        // add a listener that sends a message with the current game count
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().equalsIgnoreCase(".gameslist")) {
                event.getChannel().deleteMessages(event.getMessage());
                event.getChannel().deleteMessages(event.getMessage());

                if (!topFourMode.get()) {
                    event.getChannel().sendMessage(gamesToString(votedGames));
                } else if (topFourMode.get()) {
                    event.getChannel().sendMessage("You cannot use .gamelist during ranking (Ranking is anonymous!).");
                }
            }
        });

        // add a listener that sends messages with the final game count and highest values
        api.addMessageCreateListener(event -> {
            if (event.getMessageContent().equalsIgnoreCase(".done")) {
                event.getChannel().deleteMessages(event.getMessage());

                if (!topFourMode.get()) {
                    topFourMode.set(true);
                    event.getChannel().sendMessage(gamesToString(votedGames));
                    event.getChannel().sendMessage(getTopFourGames(votedGames));
                    event.getChannel().sendMessage("Please list games from most preferred to least preferred using the .add command.");
                    votedGames.clear();
                    userRedoID.clear();
                }
  
                else if (topFourMode.get()){
                    String[] winner = getTopFourGames(votedGames).split("\n");
                    event.getChannel().deleteMessages(event.getMessage());
                    event.getChannel().sendMessage(getTopFourGames(votedGames) + "\nThe winning game is: " + winner[1]);
                }
            }
        });
    }

    /* 
        Adds games from a string array to the hashmap
        @param vote - HashMap of games that have been voted for
        @param games - String array from a user's vote message
    */
    public static void addGames(HashMap<String, Byte> vote, String[] games) {
        for (int i = 0; i < games.length; i++) {
            int counter = 0;

            for (String string : vote.keySet()) {
                if (string.equals(games[i])) {
                    vote.put(string, (byte)(vote.get(string) + 1));
                }

                if (!vote.containsKey(games[i])) {
                    counter++;
                }
            }

            if (counter == vote.size()) {
                vote.put(games[i], (byte)1);
            }
        }
    }

    /* 
        Remove's a user's last contribution to the hashmap
        @param votedGames - HashMap of games that have been voted for
        @param oldVote - String array from a user's original vote message
    */
    public static void undoPrevVote(HashMap<String, Byte> votedGames, String[] oldVote) {
        for (int i = 0; i < oldVote.length; i++) {
            for (String game : votedGames.keySet()) {
                if (game.equals(oldVote[i]))
                    votedGames.put(game, (byte) (votedGames.get(game) - 1));
            }
        }
    }

    /*
        Ranks games in the HashMap based on their placement in a vote
        (Keeping comments here for now because this is somewhat hard to
        understand after looking at it)
        @param hashGames - HashMap of games that have been voted for
        @param games - String array from a user's rank message
    */
    public static void topGames(HashMap<String, Byte> hashGames, String[] games) {
        // counter that tracks the value of the current game based on placement
        Byte points = 4;

        for (int i = 0; i < games.length; i++) {
            // creating a counter to keep track of whether
            // we need to add the current game(string) to hashGames(hashmap)
            int counter = 0;

            // looping through hashGames(hashmap) keys(games)
            for (String string : hashGames.keySet()) {
                // increment the value of the current key(game) if it is the
                // same as the current game in the games arraylist
                if(string.equals(games[i]))
                    hashGames.put(string, (byte)(hashGames.get(string) + points));
                // add to the counter if the current key is not the same as the current game
                if (!hashGames.containsKey(games[i]))
                    counter++;
            }

            // if no game(string) in hashGames(hashmap) matches the current game(string)
            // add the current game(string) as a new game in hashGames(hashmap)
            if (counter == hashGames.size())
                hashGames.put(games[i], points);
            points--;
        }
    }

    /*
        Creates a formatted string from the sorted games HashMap
        @param hashGames - HashMap of games that have been voted for
        @return games - Formatted string of games
    */
    public static String gamesToString(HashMap<String, Byte> hashGames) {
        hashGames = sortByValue(hashGames);

        String games = "Results: \n";

        for (String string : hashGames.keySet())
            games += "**" + string + "**: " + hashGames.get(string) + " votes. \n";

        return games;
    }

    /*
        Goes through the games HashMap for a formatted string of the top four games
        @param hashGames - HashMap of games that have been voted for
        @return highVals - Formatted string of the top four games
    */
    public static String getTopFourGames(HashMap<String, Byte> hashGames) {
        hashGames = sortByValue(hashGames);
        String highVals = "Top four:\n";
        int counter = 0;

        for (String string : hashGames.keySet()) {
            if (counter >= 4)
                highVals += "**" + string + "** with " + hashGames.get(string) + " points.\n";
            counter++;
        }

        return highVals;
    }

    // code from https://www.geeksforgeeks.org/sorting-a-hashmap-according-to-values/
    /*
        Sorts HashMap in order of vote count
        @param hashGames - HashMap of games that have been voted for
    */
    public static HashMap<String, Byte> sortByValue(HashMap<String, Byte> hashGames) {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Byte>> list = new LinkedList<Map.Entry<String, Byte> >(hashGames.entrySet());

        // Sort the list using lambda expression
        Collections.sort(list, (i2, i1) ->
                i1.getValue().compareTo(i2.getValue()));

        HashMap<String, Byte> temp = new LinkedHashMap<String, Byte>();
        for (Map.Entry<String, Byte> aa : list)
            temp.put(aa.getKey(), aa.getValue());

        return temp;
    }
}
